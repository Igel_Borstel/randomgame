#include "../../include/event/EventDispatcher.hpp"
#include <typeinfo>

namespace event
{
	void EventDispatcher::runningOrNot()
	{
		std::mutex localmutex;
		std::unique_lock<std::mutex> lock(localmutex);
		while (!running && !terminating)
		{
			pausingCondition.wait(lock);
		}
	}

	void EventDispatcher::registerListener(EventListenerPtr listener, std::string eventname)
	{
		std::lock_guard<std::mutex> lock(mtx);
		auto iterator = listeners.insert(std::pair<std::string, EventListenerPtr>(eventname, listener));
		listenerAddressMap.insert(std::pair<EventListenerPtr, ListenersMapIterator>(listener, iterator));
	}

	void EventDispatcher::unregisterListener(EventListenerPtr listener)
	{
		std::lock_guard<std::mutex> lock(mtx);
		auto eventRegistrations = listenerAddressMap.equal_range(listener);
		for (auto iterator = eventRegistrations.first; iterator != eventRegistrations.second; ++iterator)
		{
			listeners.erase(iterator->second);
		}
		listenerAddressMap.erase(listener);
	}

	void EventDispatcher::send(BaseEvent& event)
	{
		std::lock_guard<std::mutex> lock(mtx);
		eventQueue.push(event);
		waitingForEvent.notify_one();
	}

	bool EventDispatcher::isRegistered(EventListenerPtr listener)
	{
		std::lock_guard<std::mutex> lock(mtx);
		return listenerAddressMap.find(listener) != listenerAddressMap.end();
	}

	void EventDispatcher::operator()()
	{
		std::mutex localmtx;
		std::unique_lock<std::mutex> lock(localmtx);
		while (!terminating)
		{
			runningOrNot();
			waitingForEvent.wait(lock);
			while (!eventQueue.empty())
			{ 
				auto eventListeners = listeners.equal_range(typeid(eventQueue.front()).name());
				for (auto iterator = eventListeners.first; iterator != eventListeners.second; ++iterator)
				{
					iterator->second->handleEvent(eventQueue.front());
				}
				eventQueue.pop();
			}
		}
		//unregister all listeners;
		listeners.clear();
		listenerAddressMap.clear();
	}

	void EventDispatcher::terminate()
	{
		terminating = true;
		pausingCondition.notify_one();
	}

	void EventDispatcher::pause()
	{
		std::lock_guard<std::mutex> lock(mtx);
		running = false;
	}

	void EventDispatcher::cont()
	{
		std::lock_guard<std::mutex> lock(mtx);
		running = true;
		pausingCondition.notify_one();
	}

	bool EventDispatcher::isRunning()
	{
		std::lock_guard<std::mutex> lock(mtx);
		return running;
	}

	bool EventDispatcher::isTerminated()
	{
		std::lock_guard<std::mutex> lock(mtx);
		return terminating;
	}
}