#include "../../include/game/Game.hpp"
#include "../../include/game/ConstantValues.hpp"

#include <iostream>

Game::Game()
	: window{ sf::VideoMode{500, 300}, "Testwindow", sf::Style::Default },
	playerShape{10.F},
	player{ math::DVec2{ 50, 100 } }
{
	playerShape.setFillColor(sf::Color::Green);
	oldTime = std::chrono::steady_clock::now();
}

void Game::loop()
{
	
	while (window.isOpen())
	{
		sf::Event event;
		sf::Vector2i temp = sf::Mouse::getPosition(window);
		mousePos = math::DVec2{ static_cast<double> (temp.x), static_cast<double> (temp.y) };
		while (window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				window.close();
				break;
			case sf::Event::MouseButtonPressed:
				if ((mousePos.getX() > player.getPosition().getX() - 10.F) && (mousePos.getX() < player.getPosition().getX() + 10.F)
					&& (mousePos.getY() > player.getPosition().getY() - 10.F) && (mousePos.getY() < player.getPosition().getY() + 10.F))
				{
					std::cout << "mouse pressed" << std::endl;
					grapPlayer = true;
				}
				break;
			case sf::Event::MouseButtonReleased:
				grapPlayer = false;
				break;
			}
			
		}
		//Tastatur input
		if ((sf::Keyboard::isKeyPressed(sf::Keyboard::A) && sf::Keyboard::isKeyPressed(sf::Keyboard::W)) || (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && sf::Keyboard::isKeyPressed(sf::Keyboard::Up)))
		{
			player.move(math::DVec2{ -3, -3 });
		}
		else if ((sf::Keyboard::isKeyPressed(sf::Keyboard::D) && sf::Keyboard::isKeyPressed(sf::Keyboard::W)) || (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && sf::Keyboard::isKeyPressed(sf::Keyboard::Up)))
		{
			player.move(math::DVec2{ 3, -3 });
		}
		else if ((sf::Keyboard::isKeyPressed(sf::Keyboard::A) && sf::Keyboard::isKeyPressed(sf::Keyboard::S)) || (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && sf::Keyboard::isKeyPressed(sf::Keyboard::Down)))
		{
			player.move(math::DVec2{ -3, 3 });
		}
		else if ((sf::Keyboard::isKeyPressed(sf::Keyboard::D) && sf::Keyboard::isKeyPressed(sf::Keyboard::S)) || (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && sf::Keyboard::isKeyPressed(sf::Keyboard::Down)))
		{
			player.move(math::DVec2{ 3, 3 });
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::A) || (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)))
		{
			player.move(math::DVec2{ -3, 0 });
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) || (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)))
		{
			player.move(math::DVec2{ 0, -3 });
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D) || (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)))
		{
			player.move(math::DVec2{ 3, 0 });
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S) || (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)))
		{
			player.move(math::DVec2{ 0, 3 });
		}
		else
		{
			player.move(math::dNull2DVector);
		}

		//Maus
		if (grapPlayer)
		{
			player.setPosition(mousePos);
		}
	
		player.update(tickCount);

		playerShape.setPosition(player.getPosition().getX(), player.getPosition().getY());
		playerShape.setOrigin(5.f, 5.f);
		window.clear(sf::Color::White);
		window.draw(playerShape);
		window.display();


		//wait until one Tick elapsed
		//TODO write solid tick clock
		auto currentTimePoint = std::chrono::steady_clock::now();
		std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(currentTimePoint - oldTime);
		while (time_span.count() < (1.F / gameconst::TICKS_PER_SECOND))
		{
			currentTimePoint = std::chrono::steady_clock::now();
			time_span = std::chrono::duration_cast<std::chrono::duration<double>>(currentTimePoint - oldTime);
		}
		oldTime = currentTimePoint;
		++tickCount;
	}
}