#include "../../include/game/Player.hpp"
#include "../../include/game/ConstantValues.hpp"

Player::Player(math::DVec2 pos)
	: position{ pos }
{}

void Player::move(math::DVec2 mov)
{
	movement = mov;
}

void Player::setPosition(math::DVec2 pos)
{
	position = pos;
}

void Player::update(long ticks)
{
	long ticksSinceLastUpdate = ticks - previousTickCount;
	auto temp = (movement * static_cast<double> (ticksSinceLastUpdate));
	position += temp;
	Entity::update(ticks);
}