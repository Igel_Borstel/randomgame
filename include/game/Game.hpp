#ifndef GAME_HPP
#define GAME_HPP

#include <SFML/Graphics.hpp>
#include <chrono>

#include "../../include/game/Player.hpp"

class Game
{
private:
	long tickCount{ 0 };

	sf::RenderWindow window;
	math::DVec2 mousePos;
	bool grapPlayer{ false };

	Player player;

	std::chrono::steady_clock::time_point oldTime;

	//temp
	sf::CircleShape playerShape;

	//temp
public:
	Game();
	void loop();
};

#endif // !GAME_HPP
