#ifndef GAME_PLAYER_HPP
#define GAME_PLAYER_HPP

#include "../math/Vec2.hpp"
#include <SFML/Graphics/Drawable.hpp>
#include "Entity.hpp"

class Player : public Entity
{
private:
	math::DVec2 position;
	math::DVec2 movement{ math::dNull2DVector };

public:
	Player(math::DVec2 pos);

	void move(math::DVec2 mov);
	void setPosition(math::DVec2 pos);
	math::DVec2 getPosition() const;

	virtual void update(long ticks) override;
};

inline math::DVec2 Player::getPosition() const { return position; }

#endif // !GAME_PLAYER_HPP
