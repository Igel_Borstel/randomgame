#ifndef GAME_ENTITY_HPP
#define GAME_ENTITY_HPP

#include "GameObject.hpp"

class Entity : public GameObject
{
protected:
	long previousTickCount{ 0 };
public:
	virtual void update(long ticks);
};

#endif // !GAME_ENTITY_HPP