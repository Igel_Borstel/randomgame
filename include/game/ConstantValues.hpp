#ifndef CONSTANTVALUES_HPP
#define CONSTANTVALUES_HPP

namespace gameconst
{
	constexpr long TICKS_PER_SECOND = 50;
}

#endif // !CONSTANTVALUES_HPP
