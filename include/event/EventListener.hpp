#ifndef EVENT_EVENTLISTENER_HPP
#define EVENT_EVENTLISTENER_HPP

#include "BaseEvent.hpp"
#include <memory>

namespace event
{
	struct EventListener
	{
		virtual void handleEvent(BaseEvent& event) = 0;
	};

	using EventListenerPtr = std::shared_ptr<EventListener>;
}

#endif // !EVENT_EVENTLISTENER_HPP
