#ifndef EVENT_BASEEVENT_HPP
#define EVENT_BASEEVENT_HPP

namespace event
{
	struct BaseEvent
	{
		virtual ~BaseEvent() = default;
	};
}

#endif // !EVENT_BASEEVENT_HPP
