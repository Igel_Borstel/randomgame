#ifndef EVENT_EVENTDISPATCHER_HPP
#define EVENT_EVENTDISPATCHER_HPP

#include <string>
#include "EventListener.hpp"
#include <mutex>
#include <condition_variable>
#include <map>
#include <queue>

namespace event
{
	class EventDispatcher
	{
		std::multimap<std::string, EventListenerPtr> listeners;
		using ListenersMapIterator = std::multimap<std::string, EventListenerPtr>::iterator;
		std::multimap <EventListenerPtr, ListenersMapIterator> listenerAddressMap;
		std::queue<BaseEvent&> eventQueue;

		//Threading
		bool running;
		bool terminating;
		bool wasStarted;

		std::mutex mtx;
		std::condition_variable pausingCondition;
		std::condition_variable waitingForEvent;
		void runningOrNot();
	public:

		virtual void registerListener(EventListenerPtr listener, std::string eventname);
		virtual void unregisterListener(EventListenerPtr listener);
		virtual void send(BaseEvent& event);
		virtual bool isRegistered(EventListenerPtr listener);

		
		virtual void operator()();
		virtual void terminate();
		virtual void pause();
		virtual void cont();

		virtual bool isRunning();
		virtual bool isTerminated();
	};
}

#endif // !EVENT_EVENTDISPATCHER_HPP
