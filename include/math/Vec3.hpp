#ifndef MATH_VEC3_HPP
#define MATH_VEC3_HPP

namespace math
{
	namespace _impl
	{
		template<typename T>
		class Vec3
		{
		private:
			T x;
			T y;
			T z;
		public:
			Vec3()
				: x{ 0 }, y{ 0 }, z{ 0 }
			{
			}

			Vec3(T _x, T _y, T _z)
				: x{ _x }, y{ _y }, z{ _z }
			{
			}

			Vec3<T> operator+(Vec3<T> vec) const
			{
				Vec3<T> temp{ x, y, z };
				temp.x += vec.x;
				temp.y += vec.y;
				temp.z += vec.z;
				return temp;
			}
			Vec3<T> operator-(Vec3<T> vec) const
			{
				Vec3<T> temp{ x, y, z };
				temp.x -= vec.x;
				temp.y -= vec.y;
				temp.z -= vec.z;
				return temp;
			}

			Vec3<T> operator*(T scalar) const
			{
				Vec3<T> temp{ x, y, z };
				temp.x *= scalar;
				temp.y *= scalar;
				temp.z *= scalar;
				return temp;
			}

			T dot(Vec3<T> vec) const
			{
				return (x * vec.x) + (y * vec.y) + (z * vec.z);
			}

			Vec3<T> cross(Vec3<T> vec) const
			{
				Vec3<T> temp{ x, y, z };
				temp.x = ((y * vec.z) - (z * vec.y));
				temp.y = ((z * vec.x) - (x * vec.z));
				temp.z = ((x * vec.y) - (y * vec.x));
				return temp;
			}

			T abs() const
			{
				return std::sqrt(((x * x) + (y * y) + (z * z)));
			}

			Vec3<T> norm() const
			{
				Vec3<T> temp{ x, y, z };
				temp.x /= abs();
				temp.y /= abs();
				temp.z /= abs();
				return temp;
			}

			Vec3<T>& operator+=(Vec3<T>& vec)
			{
				x += vec.x;
				y += vec.y;
				z += vec.z;
				return *this;
			}

			Vec3<T>& operator-=(Vec3<T>& vec)
			{
				x -= vec.x;
				y -= vec.y;
				z -= vec.z;
				return *this;
			}

			Vec3<T>& operator*=(T& scalar)
			{
				x *= scalar;
				y *= scalar;
				z *= scalar;
				return *this;
			}

			const T getX() const
			{
				return x; const
			}

			const T getY() const
			{
				return y;
			}

			const T getZ() const
			{
				return z;
			}
		};
	}

	using Vec3 = _impl::Vec3<float>;
	using DVec3 = _impl::Vec3<double>;
	using IVec3 = _impl::Vec3<int>;
	using UIVec3 = _impl::Vec3<unsigned int>;


	const Vec3 nullVector{ 0.f, 0.f, 0.f };
	const DVec3 dNullVector{ 0., 0., 0. };
	const IVec3 iNullVector{ 0, 0, 0 };
	const UIVec3 uiNullVector{ 0, 0, 0 };

	const Vec3 baseXVector{ 1.f, 0.f, 0.f };
	const Vec3 baseYVector{ 0.f, 1.f, 0.f };
	const Vec3 baseZVector{ 0.f, 0.f, 1.f };

	const DVec3 dBaseXVector{ 1., 0., 0. };
	const DVec3 dBaseYVector{ 0., 1., 0. };
	const DVec3 dBaseZVector{ 0., 0., 1. };

	const IVec3 iBaseXVector{ 1, 0, 0 };
	const IVec3 iBaseYVector{ 0, 1, 0 };
	const IVec3 iBaseZVector{ 0, 0, 1 };

	const UIVec3 uiBaseXVector{ 1, 0, 0 };
	const UIVec3 uiBaseYVector{ 0, 1, 0 };
	const UIVec3 uiBaseZVector{ 0, 0, 1 };
}

#endif // !MATH_VEC3_HPP