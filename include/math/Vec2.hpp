#ifndef MATH_VEC2_HPP
#define MATH_VEC2_HPP

#include "Vec3.hpp"

namespace math
{
	namespace _impl
	{
		template<typename T>
		class Vec2
		{
		private:
			T x;
			T y;
		public:
			Vec2()
				: x{ 0 }, y{ 0 }
			{
			}

			Vec2(T _x, T _y)
				: x{ _x }, y{ _y }
			{
			}

			Vec2<T> operator+(Vec2<T> vec) const
			{
				Vec3<T> temp{ x, y };
				temp.x += vec.x;
				temp.y += vec.y;
				return temp;
			}
			Vec2<T> operator-(Vec2<T> vec) const
			{
				Vec2<T> temp{ x, y };
				temp.x -= vec.x;
				temp.y -= vec.y;
				return temp;
			}

			Vec2<T> operator*(T scalar) const
			{
				Vec2<T> temp{ x, y };
				temp.x *= scalar;
				temp.y *= scalar;
				return temp;
			}

			T dot(Vec2<T> vec) const
			{
				return (x * vec.x) + (y * vec.y);
			}

			Vec3<T> cross(Vec2<T> vec) const
			{
				Vec3<T> temp{ x, y, 0 };
				temp.x = ((y * 0) - (z * vec.y));
				temp.y = ((0 * vec.x) - (x * 0));
				temp.z = ((x * vec.y) - (y * vec.x));
				return temp;
			}

			T abs() const
			{
				return std::sqrt(((x * x) + (y * y)));
			}

			Vec2<T> norm() const
			{
				Vec2<T> temp{ x, y };
				temp.x /= abs();
				temp.y /= abs();
				return temp;
			}

			Vec2<T>& operator+=(Vec2<T>& vec)
			{
				x += vec.x;
				y += vec.y;
				return *this;
			}

			Vec2<T>& operator-=(Vec2<T>& vec)
			{
				x -= vec.x;
				y -= vec.y;
				return *this;
			}

			Vec2<T>& operator*=(T& scalar)
			{
				x *= scalar;
				y *= scalar;
				return *this;
			}

			const T getX() const
			{
				return x;
			}

			const T getY() const
			{
				return y;
			}
		};
	}

	using Vec2 = _impl::Vec2<float>;
	using DVec2 = _impl::Vec2<double>;
	using IVec2 = _impl::Vec2<int>;
	using UIVec2 = _impl::Vec2<unsigned int>;


	const Vec2 null2DVector{ 0.f, 0.f };
	const DVec2 dNull2DVector{ 0., 0. };
	const IVec2 iNull2DVector{ 0, 0 };

	const Vec2 baseX2DVector{ 1.f, 0.f };
	const Vec2 baseY2DVector{ 0.f, 1.f };

	const DVec2 dBaseX2DVector{ 1., 0. };
	const DVec2 dBaseY2DVector{ 0., 1. };

	const IVec2 iBaseX2DVector{ 1, 0 };
	const IVec2 iBaseY2DVector{ 0, 1 };

	const UIVec2 uiBaseX2DVector{ 1, 0 };
	const UIVec2 uiBaseY2DVector{ 0, 1 };
}
#endif // !MATH_VEC2_HPP


